<%@ include file="/header.jsp" %>
<h1>Counter</h1>


<%
    final String codeString = request.getParameter("sleep");
    long sleep = 200l;
    if (codeString != null)
    {
        sleep = Long.valueOf(codeString);
    }
%>
<%

final String countString = request.getParameter("count");
int count = 1;
if (countString != null) {
  count = Integer.parseInt(countString);
}

for (int i=0;i<count;i++) {
    out.print(i);
    out.println(" <br>");
    out.flush();
    
    Thread.sleep(sleep);
}
%>

<form name="input" method="get">
    Sleep: <input type="number" value="<%=sleep%>" size="3" name="sleep">
    Count: <input type="number" value="<%=count%>" size="3" name="count">
    <input type="submit" value="Send">
</form>

<%@ include file="/footer.jsp" %>
