<%
    final int[] arr = new int[5];
    try {
        // trigger a array index out of bounds
        arr[10] = 50;
    } finally {
        arr[1] = 1;
    }
%>