<%@ include file="/header.jsp" %>
<h1>Fib</h1>

<%
final String threadsString = request.getParameter("threads");
int threads = 1;
if (threadsString != null) {
  threads = Integer.parseInt(threadsString);
}

final String fibString = request.getParameter("fib");
int fib = 3;
if (fibString != null) {
  fib = Integer.parseInt(fibString);

  final int f = fib;
  final javax.servlet.jsp.JspWriter o = out;
  java.util.List l = new java.util.ArrayList();

  for (int i=0;i<threads;i++) {
    final int id = i;
    Thread thread = new Thread("fib("+id+")") {
      public void run() {
        try {
          long start = System.currentTimeMillis();
          com.intergral.fusionreactor.test.Fibber fib = new com.intergral.fusionreactor.test.Fibber();
          long result = fib.fib(f);
          long duration = System.currentTimeMillis() - start;
          o.println("Thread ID = " + id + " : Duration = " + duration + "ms : Result = " + result + "<br>");
        } catch (Exception e) {
          e.printStackTrace();
        }
      }};
    l.add(thread);
    thread.start();
  }

  for (int i=0;i<threads;i++) {
    ((Thread)l.get(i)).join();
  }
%>
<br>
<%
}
%>

<form name="input" method="get">
  Threads: <input type="number" value="<%=threads%>" size="1" name="threads"><br>
  Fib (n): <input type="number" value="<%=fib%>" size="3" name="fib">
  <input type="submit" value="Calculate">
</form>

<%@ include file="/footer.jsp" %>
