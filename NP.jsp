<%
    // page will receive an argument and perform a str length operation on that argument.
    // exception thrown if no argument passed in

    String arg = request.getParameter("arg");;

    // triggers a null pointer
    out.println("STRING LENGTH = ");
    out.println( arg.length());
    out.println("<BR><BR>");
    out.println("STRING (ARG) = ");
    out.println( arg );
%>